@extends('master')

@section('content')

  @if(session('message'))
    <div class="alert alert-success">
      {{session('message')}}
    </div>
  @endif
  <table class="table table-striped table-hover table-bordered" style="margin-top:30px">
    <tr>
      <th>Name</th>
      <th>Actions</th>
    </tr>
    @foreach($students as $student)
      <tr>
        <td>{{ucfirst($student->name)}}</td>
        <td><a href="<?php echo route('pages.edit', array('id' => $student->id)) ?>" class="button-clear"><span><i class="fa fa-pencil"></i>&nbsp; Edit</span></a> &nbsp; &nbsp; 
        	<a href="javascript:void(0);" onclick="deleteRecord('<?php echo route('pages.delete', array('id' => $student->id)) ?>')" class="button"><span><i class="fa fa-trash-o"></i>&nbsp; Delete</span></a>
        </td>
      </tr>
    @endforeach
  </table>

  <a href="<?php echo route('pages.add') ?>" class="button"><i class="fa fa-plus"></i>&nbsp; Add Student</a>
@stop

@section('script')

 <script type="text/javascript">
  function deleteRecord(url){
    var sure = confirm("Are you sure you want to delete this record?");

    if(sure){
      window.location = url;
    }
    else{
      return false;
	}
  }
  </script>
@stop