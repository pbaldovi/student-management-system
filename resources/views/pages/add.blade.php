@extends('master')

@section('content')

@if (count($errors) > 0)
    <div class="alert alert-danger">
      <h4>Failed because:</h4>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<h4>Add Student</h4>
  <form style="margin-top:30px" method="POST" action="<?php echo route('pages.store') ?>">
  	<?php echo csrf_field(); ?>
  	<div class="form-group">
      <label for="name">Name: </label><input type="text" name="name" value="{{old('name')}}" class="form-control" id="name" required>
    </div>
    <div class="form-group">
      <label for="email">Email: </label><input type="email" name="email" value="{{old('email')}}" class="form-control" id="email" required>
    </div>
    <div class="form-group">
      <label for="address">Address: </label><textarea id="address" name="address" class="form-control" required>{{old('address')}}</textarea>
	</div>
	Gender: &nbsp;
  <div class="radio">
      <label for="male"> <input type="radio" id="male" name="gender" value="male" required > Male</label><br>
      <label for="female"> <input type="radio" id="female" name="gender" value="female"> Female</label>
    </div>
    <div class="form-group">
      <label for="eyop">Expected year of passing:</label>
      <select id="eyop" name="eyop" class="form-control" required>
        <option value="2014">2014</option>
        <option value="2015">2015</option>
        <option value="2016">2016</option>
        <option value="2017">2017</option>
        <option value="2018">2018</option>
      </select>
    </div>
    <div class="form-group">
      <h4>Interests:<h4>
      @foreach($interests as $interest)
      <label class="checkbox-inline">
        <input type="checkbox" name="interests[]" value="{{$interest->id}}" >{{ucfirst($interest->interestName)}}
      </label>
      @endforeach
    </div>
    <br><br><br>
    <a href="<?php echo route('pages.home') ?>" class="button-clear"><span>Cancel</span></a>&nbsp;&nbsp;
    <input type="submit" value="Add" class="button">
  </form>

  @stop

