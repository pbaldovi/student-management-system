@extends('master')

@section('content')

@if (count($errors) > 0)
    <div class="alert alert-danger">
      <h4>Updation failed because:</h4>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

  <h4>Edit Student Details</h4>
  <form style="margin-top:30px" method="POST" action="<?php echo route('pages.update') ?>">
  	<?php echo csrf_field(); ?>
    <input type="hidden" name="id" value="{{$student->id}}"> 
  	<div class="form-group">
      <label for="name">Name: </label><input type="text" name="name" value="{{$student->name}}" class="form-control" id="name" required>
    </div>
    <div class="form-group">
      <label for="email">Name: </label><input type="email" name="email" value="{{$student->email}}" class="form-control" id="email" required>
    </div>
    <div class="form-group">
      <label for="address">Address: </label><textarea id="address" name="address" class="form-control" required>{{$student->address}}</textarea>
	</div>
  <br>
  Gender: &nbsp;
	<div class="radio">
      <label for="male"> <input type="radio" id="male" name="gender" value="male" <?php if($student->gender == 'male') echo "checked"; ?> required > Male</label><br>
      <label for="female"> <input type="radio" id="female" name="gender" value="female" <?php if($student->gender == 'female') echo "checked"; ?> > Female</label>
    </div>
    <div class="form-group">
      <label for="eyop">Expected year of passing:</label>
      <select id="eyop" name="eyop" class="form-control" required>
      	<?php $eyop = $student->eyop ?>
        <option value="2014" <?php if($eyop == 2014) echo 'selected="selected"' ?> >2014</option>
        <option value="2015" <?php if($eyop == 2015) echo 'selected="selected"' ?> >2015</option>
        <option value="2016" <?php if($eyop == 2016) echo 'selected="selected"' ?> >2016</option>
        <option value="2017" <?php if($eyop == 2017) echo 'selected="selected"' ?> >2017</option>
        <option value="2018" <?php if($eyop == 2018) echo 'selected="selected"' ?> >2018</option>
      </select>
    </div>
    <div class="form-group">
      <h4>Interests:<h4>
      <?php 
        foreach($student->interests as $interest){
          $interests_array[] = $interest->id;
        }
      ?>
      @foreach($interests as $interest)
      <label class="checkbox-inline">
        <input type="checkbox" name="interests[]" value="{{$interest->id}}" <?php if(in_array($interest->id, $interests_array)) echo 'checked' ?> >{{ucfirst($interest->interestName)}}
      </label>
      @endforeach
    </div>
    <br><br><br>
    <a href="<?php echo route('pages.home') ?>" class="button-clear"><span>Cancel</span></a>&nbsp;&nbsp;
    <input type="submit" value="Update" class="button">
  </form>

@stop
