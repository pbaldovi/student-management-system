<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Student Management System</title>
    <link rel="stylesheet" type="text/css" href="<?php echo route('pages.home').'/css/compiled/theme.css' ?>">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
  </head>

  <body>
    <header class="navbar navbar-inverse normal" role="banner">
      <div class="container">
        <div class="navbar-header">
            <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".bs-navbar-collapse">
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a href="<?php echo route('pages.home') ?>" class="navbar-brand">BTE Engineering College</a>
        </div>
        <nav class="collapse navbar-collapse bs-navbar-collapse" role="navigation">
            <ul class="nav navbar-nav navbar-right">
              <li>
                <a href="<?php echo route('pages.add') ?>">
                    <i class="fa fa-plus-square"></i> &nbsp; Add Student
                </a>
              </li>
              <li>
                <a href="<?php echo route('pages.home') ?>">
                    <i class="fa fa-list"></i> &nbsp; List All Students
                </a>
              </li>
            </ul>
        </nav>
      </div>
    </header>
    <div class="container" style="min-height:400px;">

    @yield('content')

    @yield('script')

    </div>

    <div id="footer">
    <div class="container">
      <div class="row">
        <div class="col-sm-6 copyright">
          &copy; BTE Engineering College
        </div>
        <div class="col-sm-6 menu">
          <ul>
                <li>
                    <a href="<?php echo route('pages.add') ?>">Add Student</a>
                </li>
                <li>
                  <a href="<?php echo route('pages.home') ?>">List All Students</a>
                </li>
              </ul>
        </div>
      </div>
    </div>
  </div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap/bootstrap.min.js"></script>
    <script src="js/theme.js"></script>
  </body>
</html>
