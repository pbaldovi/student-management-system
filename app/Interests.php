<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Interests extends Model
{
    //no need for timestamps
    public $timestamps = false;

    public function interests(){
      return $this->hasMany('App\Students', 'id');
    }
}