<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Students;
use App\Interests;

//TODO:
//Eloquent only ??done
//mass assignment ??done-doubt
//Separate table for interests ?done
//Student model, interest model, m2m - amit, saumya, manil ?done
//GIT - pull, push, stash, branch, clone, commit, checkout ==> Bitbucket. ?done
//React ?done
//Flash messages ?done
//Restful URL's and functions ?done
//E-mail field => uniqueness check in js. ??done

class StudentController extends Controller
{
    public function index(){
      $students = Students::getStudents();
      return view('pages.home', compact('students'));
    }

    public function add(){
      $interests = Interests::all();
      return view('pages.add', compact('interests'));
    }

    public function store(Request $request){ 
      $this->validate($request, [
        'name' => 'required',
        'email' => 'required|unique:students',
        'address' => 'required',
        'gender' => 'required',
        'eyop' => 'required',
        'interests' => 'required'
      ]);

      //if validation is successful following code will run 
      //and data will be stored. 
      //Otherwise laravel will automatically redirect to previous page.

      $student = new Students;
      $student->createStudent($request);
      
      $message = "New student ".$request->get('name')." added.";
      return redirect('/')->with('message', $message);
    }

    public function delete($id){
      $student = Students::find($id);
      $studentName = $student->name;
      $student->deleteStudent();
      return redirect('/')->with('message', 'Student '.$studentName.' deleted!!');
    }

    public function edit($id){
      $student = Students::find($id);
      $interests = Interests::all();
      return view('pages.edit', compact('student', 'interests'));
    }

    public function update(Request $request){ 
      $this->validate($request, [
        'name' => 'required',
        'email' => 'required',
        'address' => 'required',
        'gender' => 'required',
        'eyop' => 'required',
        'interests' => 'required'
      ]);

      $id = $request->id;
      $student = Students::find($id);
      $student->updateStudent($request);
      
      $message = "Edit successful";
      return redirect('/')->with('message', $message);
    }
}
