<?php


Route::get('/', array('as' => 'pages.home', 'uses' => 'StudentController@index'));

Route::get('add', array('as' => 'pages.add', 'uses' => 'StudentController@add'));

Route::post('store', array('as' => 'pages.store', 'uses' => 'StudentController@store'));

Route::get('delete/{id}', array('as' => 'pages.delete', 'uses' => 'StudentController@delete'));

Route::get('edit/{id}', array('as' => 'pages.edit', 'uses' => 'StudentController@edit'));

Route::post('update', array('as' => 'pages.update', 'uses' => 'StudentController@update'));

