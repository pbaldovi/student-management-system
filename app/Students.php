<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Students extends Model
{
    //no need for timestamps
    public $timestamps = false;
    //prevent mass assignment of interest in students table
    protected $guarded = ['interests'];

    //Many to Many relationship with Interests table
    public function interests(){
      return $this->belongsToMany('App\Interests');
    }

    //get all students from student table
    public static function getStudents(){
      $students = Students::all();
      return $students;
    }

    //creates new student
    public function createStudent($request){
      $student = Students::create($request->all());
      $student->interests()->sync($request->input('interests'));
    }

    //deletes student
    public function deleteStudent()
	{
		$this->interests()->detach();
		$this->delete();
	}

    //update student record
	public function updateStudent($request){
	  $input = $request->all();
	  $student = $this->fill($input)->save();
	  $this->interests()->sync($request->input('interests'));
	  return $student;

	}
}
